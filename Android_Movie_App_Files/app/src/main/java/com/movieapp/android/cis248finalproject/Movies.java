package com.movieapp.android.cis248finalproject;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */

public class Movies {

    public int movieID;
    public String movieTitle;
    public int releaseDate;
    public int movieRuntime;
    public String movieRating;
    public String moviePosterUrl;
    public String movieDescription;
    public String movieTrailer1;
    public String movieTrailer2;

    public Movies(){
        movieTitle = "";
        movieID = 0;
        releaseDate = 0;
        movieRuntime = 0;
        movieRating = "";
        moviePosterUrl = "";
        movieDescription = "";
        movieTrailer1 = "";
        movieTrailer2 = "";
    }

    public int getMovieID(){
        return movieID;
    }

    public String getMovieTitle(){
        return movieTitle;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public int getMovieRuntime(){
        return movieRuntime;
    }

    public String getMovieRating(){
        return movieRating;
    }

    public String getMoviePosterUrl(){
        return moviePosterUrl;
    }

    public String getMovieDescription(){
        return movieDescription;
    }

    public String movieTrailer1(){
        return movieTrailer1;
    }

    public String movieTrailer2(){
        return movieTrailer2;
    }

    public void setMovieTitle(String movieTitle){
        this.movieTitle = movieTitle;
    }

    public void setReleaseDate(int releaseDate){
        this.releaseDate = releaseDate;
    }

    public void setMovieRuntime(int movieRuntime){
        this.movieRuntime = movieRuntime;
    }

    public void setMovieRating(String movieRating){
        this.movieRating = movieRating;
    }

    public void setMoviePosterUrl(String moviePosterUrl){
        this.moviePosterUrl = moviePosterUrl;
    }

    public void setMovieDescription(String movieDescription){
        this.movieDescription = movieDescription;
    }

    public void setMovieTrailer1(String movieTrailer1){
        this.movieTrailer1 = movieTrailer1;
    }

    public void setMovieTrailer2(String movieTrailer2){
        this.movieTrailer2 = movieTrailer2;
    }

    public void setMovieID(int movieID){
        this.movieID = movieID;
    }
}
