package com.movieapp.android.cis248finalproject;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */

public class PosterImages {

    private String poster_image_url;

    public String getPoster_image_url() {
        return poster_image_url;
    }

    public void setPoster_image_url(String poster_image_url) {
        this.poster_image_url = poster_image_url;
    }
}
