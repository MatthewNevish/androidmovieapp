package com.movieapp.android.cis248finalproject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.movieapp.android.cis248finalproject.R.layout.main_activity2;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */


public class MainActivity2 extends AppCompatActivity implements View.OnClickListener{

    private Context context;

    final static String baseURL = "http://image.tmdb.org/t/p/w500/";

    final int SIZE = 20;

    ImageButton favoriteButton;
    ImageButton trailerButton1;
    ImageButton trailerButton2;

    String movieTitle;
    int movieReleaseDate;
    int movieRuntime;
    String moviePosterUrl;
    String movieDescription;
    String movieTrailerUrl1;
    String movieTrailerUrl2;
    String movieRating;


    Movies[] movieList = new Movies[SIZE];

    final static String movieTitles[] = {
            "Underworld: Blood Wars",
            "Fantastic Beasts and Where to Find Them",
            "Suicide Squad",
            "Arrival",
            "Miss Peregrine's Home for Peculiar Children",
            "Sully",
            "Mad Max: Fury Road",
            "Captain America: Civil War",
            "Moana",
            "Interstellar",
            "The Secret Life of Pets",
            "Doctor Strange",
            "Jason Bourne",
            "Rogue One: A Star Wars Story",
            "Insurgent",
            "John Wick",
            "Jurassic World",
            "The Magnificent Seven",
            "The Hunger Games: Mockingjay - Part 1",
            "Finding Dory",
    };

    final static int movieIDs[] = {
            346672,
            259316,
            297761,
            329865,
            283366,
            363676,
            76341,
            271110,
            277834,
            157336,
            328111,
            284052,
            324668,
            330459,
            262500,
            245891,
            135397,
            333484,
            131631,
            127380
    };

    final static int releaseDates[] = {
            2016,
            2016,
            2016,
            2016,
            2016,
            2016,
            2015,
            2016,
            2016,
            2014,
            2016,
            2016,
            2016,
            2016,
            2015,
            2014,
            2015,
            2016,
            2014,
            2016
    };

    final static int movieRuntimes[] = {
            91,
            133,
            123,
            116,
            127,
            96,
            120,
            147,
            107,
            169,
            87,
            115,
            123,
            134,
            119,
            101,
            124,
            133,
            123,
            97
    };

    final static String movieRatings[] = {
            "6.3/10",
            "7.8/10",
            "6.5/10",
            "8.3/10",
            "6.9/10",
            "7.6/10",
            "8.1/10",
            "8.0/10",
            "8.1/10",
            "8.6/10",
            "6.6/10",
            "7.9/10",
            "6.7/10",
            "8.1/10",
            "6.3/10",
            "7.2/10",
            "7.0/10",
            "7.1/10",
            "6.7/10",
            "7.5/10"
    };

    final static String moviePosterUrls[] = {
            "/nHXiMnWUAUba2LZ0dFkNDVdvJ1o.jpg",
            "/gri0DDxsERr6B2sOR1fGLxLpSLx.jpg",
            "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg",
            "/xGgg2UI20qtEh7mura3RRwP8d3I.jpg",
            "/vR9YvUJCead23MOWwVzv9776eb1.jpg",
            "/h6O5OE3ueRVdCc7V7cwTiQocI7D.jpg",
            "/kqjL17yufvn9OVLyXYpvtyrFfak.jpg",
            "/5N20rQURev5CNDcMjHVUZhpoCNC.jpg",
            "/z4x0Bp48ar3Mda8KiPD1vwSY3D8.jpg",
            "/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg",
            "/WLQN5aiQG8wc9SeKwixW7pAR8K.jpg",
            "/xfWac8MTYDxujaxgPVcRD9yZaul.jpg",
            "/lFSSLTlFozwpaGlO31OoUeirBgQ.jpg",
            "/qjiskwlV1qQzRCjpV0cL9pEMF9a.jpg",
            "/6w1VjTPTjTaA5oNvsAg0y4H6bou.jpg",
            "/b9uYMMbm87IBFOq59pppvkkkgNg.jpg",
            "/jjBgi2r5cRt36xF6iNUEhzscEcb.jpg",
            "/cparcxTFuHdlSOP3MJOpN7Ec9NB.jpg",
            "/gj282Pniaa78ZJfbaixyLXnXEDI.jpg",
            "/z09QAf8WbZncbitewNk6lKYMZsh.jpg",
    };

    final static String movieDescriptions[] = {
            "Underworld: Blood Wars follows Vampire death dealer, Selene, as she fends off brutal attacks from both the Lycan clan and the Vampire faction that betrayed her. With her only allies, David and his father Thomas, she must stop the eternal war between Lycans and Vampires, even if it means she has to make the ultimate sacrifice.",
            "In 1926, Newt Scamander arrives at the Magical Congress of the United States of America with a magically expanded briefcase, which houses a number of dangerous creatures and their habitats. When the creatures escape from the briefcase, it sends the American wizarding authorities after Newt, and threatens to strain even further the state of magical and non-magical relations.",
            "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
            "Taking place after alien crafts land around the world, an expert linguist is recruited by the military to determine whether they come in peace or are a threat.",
            "A teenager finds himself transported to an island where he must help protect a group of orphans with special powers from creatures intent on destroying them.",
            "On 15 January 2009, the world witnessed the 'Miracle on the Hudson' when Captain 'Sully' Sullenberger glided his disabled plane onto the frigid waters of the Hudson River, saving the lives of all 155 aboard. However, even as Sully was being heralded by the public and the media for his unprecedented feat of aviation skill, an investigation was unfolding that threatened to destroy his reputation and career.",
            "An apocalyptic story set in the furthest reaches of our planet, in a stark desert landscape where humanity is broken, and most everyone is crazed fighting for the necessities of life. Within this world exist two rebels on the run who just might be able to restore order. There's Max, a man of action and a man of few words, who seeks peace of mind following the loss of his wife and child in the aftermath of the chaos. And Furiosa, a woman of action and a woman who believes her path to survival may be achieved if she can make it across the desert back to her childhood homeland.",
            "Following the events of Age of Ultron, the collective governments of the world pass an act designed to regulate all superhuman activity. This polarizes opinion amongst the Avengers, causing two factions to side with Iron Man or Captain America, which causes an epic battle between former allies.",
            "In Ancient Polynesia, when a terrible curse incurred by Maui reaches an impetuous Chieftain's daughter's island, she answers the Ocean's call to seek out the demigod to set things right.",
            "Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.",
            "The quiet life of a terrier named Max is upended when his owner takes in Duke, a stray whom Max instantly dislikes.",
            "After his career is destroyed, a brilliant but arrogant surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.",
            "The most dangerous former operative of the CIA is drawn out of hiding to uncover hidden truths about his past.",
            "A rogue band of resistance fighters unite for a mission to steal the Death Star plans and bring a new hope to the galaxy.",
            "Beatrice Prior must confront her inner demons and continue her fight against a powerful alliance which threatens to tear her society apart.",
            "After the sudden death of his beloved wife, John Wick receives one last gift from her, a beagle puppy named Daisy, and a note imploring him not to forget how to love. But John's mourning is interrupted when his 1969 Boss Mustang catches the eye of sadistic thug Iosef Tarasov who breaks into his house and steals it, beating John unconscious in the process. Unwittingly, he has just reawakened one of the most brutal assassins the underworld has ever known.",
            "Twenty-two years after the events of Jurassic Park, Isla Nublar now features a fully functioning dinosaur theme park, Jurassic World, as originally envisioned by John Hammond.",
            "Seven gun men in the old west gradually come together to help a poor village against savage thieves.",
            "Katniss Everdeen reluctantly becomes the symbol of a mass rebellion against the autocratic Capitol.",
            "Dory is reunited with her friends Nemo and Marlin in the search for answers about her past. What can she remember? Who are her parents? And where did she learn to speak Whale?",
    };

    final static String movieTrailers1[] = {
            "https://youtu.be/rKHL5PyAPzs",
            "https://youtu.be/Vso5o11LuGU",
            "https://youtu.be/CmRih_VtVAs",
            "https://youtu.be/tFMo3UJ4B4g",
            "https://youtu.be/tV_IhWE4LP0",
            "https://youtu.be/mjKEXxO2KNE",
            "https://youtu.be/hEJnMQG9ev8",
            "https://youtu.be/xnv__ogkt0M",
            "https://youtu.be/LKFuXETZUsI",
            "https://youtu.be/2LqzF5WauAw",
            "https://youtu.be/UZ4WBlveGfw",
            "https://youtu.be/Lt-U_t2pUHI",
            "https://youtu.be/F4gJsKZvqE4",
            "https://youtu.be/frdj1zb9sMY",
            "https://youtu.be/Vzn4MJdaabw",
            "https://youtu.be/2AUmvWm5ZDQ",
            "https://youtu.be/RFinNxS5KN4",
            "https://youtu.be/q-RBA0xoaWU",
            "https://youtu.be/3PkkHsuMrho",
            "https://youtu.be/dLIy1K8kJPo",
    };

    final static String movieTrailers2[] = {
            "https://youtu.be/FtbM_W9iNjg",
            "https://youtu.be/YdgQj7xcDJo",
            "https://youtu.be/SULFiCh0Zsw",
            "https://youtu.be/ZLO4X6UI8OY",
            "https://youtu.be/DN1uhnnKscY",
            "https://youtu.be/6Tbkbx4Hz8Q",
            "https://youtu.be/YWNWi-ZWL3c",
            "https://youtu.be/dKrVegVI0Us",
            "https://youtu.be/Ljik3zsGNF4",
            "https://youtu.be/Lm8p5rlrSkY",
            "https://youtu.be/UZJVc_JTI_w",
            "https://youtu.be/HSzx-zryEgM",
            "https://youtu.be/buT_1-xk-dM",
            "https://youtu.be/sC9abcLLQpI",
            "https://youtu.be/suZcGoRLXkU",
            "https://youtu.be/uRkDFEXnZwA",
            "https://youtu.be/aJJrkyHas78",
            "https://youtu.be/deSRpSn8Pyk",
            "https://youtu.be/IXshQ5mv1K8",
            "https://youtu.be/kZrAKYAF3y8",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(main_activity2);

        for (int i = 0; i < movieList.length; i++) {
            Movies movie = new Movies();
            movie.setMovieTitle(movieTitles[i]);
            movie.setMovieID(movieIDs[i]);
            movie.setReleaseDate(releaseDates[i]);
            movie.setMovieRuntime(movieRuntimes[i]);
            movie.setMoviePosterUrl(moviePosterUrls[i]);
            movie.setMovieDescription(movieDescriptions[i]);
            movie.setMovieRating(movieRatings[i]);
            movie.setMovieTrailer1(movieTrailers1[i]);
            movie.setMovieTrailer2(movieTrailers2[i]);
            movieList[i] = movie;
        }

        Movies currentMovie = new Movies();

        int movieID = getIntent().getIntExtra("movieID", 0);


        for (int i = 0; i < movieIDs.length; i++) {
            if (movieID == movieList[i].getMovieID()) {
                currentMovie = movieList[i];
            }
        }

        movieTitle = currentMovie.getMovieTitle();
        movieReleaseDate = currentMovie.getReleaseDate();
        movieRuntime = currentMovie.getMovieRuntime();
        moviePosterUrl = currentMovie.getMoviePosterUrl();
        movieDescription = currentMovie.getMovieDescription();
        movieTrailerUrl1 = currentMovie.movieTrailer1();
        movieTrailerUrl2 = currentMovie.movieTrailer2();
        movieRating = currentMovie.getMovieRating();

        context = this;

        favoriteButton = (ImageButton) findViewById(R.id.favorite_button);
        favoriteButton.setOnClickListener(this);
        trailerButton1 = (ImageButton) findViewById(R.id.trailer_button1);
        trailerButton1.setOnClickListener(this);
        trailerButton2 = (ImageButton) findViewById(R.id.trailer_button2);
        trailerButton2.setOnClickListener(this);

        TextView movieTitleBar = (TextView) findViewById(R.id.movie_title);
        movieTitleBar.setText(movieTitle);

        ImageView posterImageView = (ImageView) findViewById(R.id.movie_poster);
        Picasso.with(context).load(baseURL + moviePosterUrl).into(posterImageView);

        TextView releaseDateView = (TextView) findViewById(R.id.release_date);
        releaseDateView.setText(String.valueOf(movieReleaseDate));

        TextView runtimeView = (TextView) findViewById(R.id.movie_runtime);
        runtimeView.setText(String.valueOf(movieRuntime) + " min");

        TextView ratingView = (TextView) findViewById(R.id.movie_rating);
        ratingView.setText("IMDB Rating: " + movieRating);

        TextView descriptionView = (TextView) findViewById(R.id.movie_description);
        descriptionView.setText(movieDescription);


    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.favorite_button:
                favoriteButton.setImageResource(R.drawable.image_button2);
                break;
            case R.id.trailer_button1:
                final Intent intent1 = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(movieTrailerUrl1));
                startActivity(intent1);
                trailerButton1.setImageResource(R.drawable.trailer_button2);
                break;
            case R.id.trailer_button2:
                final Intent intent2 = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(movieTrailerUrl1));
                startActivity(intent2);
                trailerButton2.setImageResource(R.drawable.trailer_button2);
                break;
        }
    }
}

