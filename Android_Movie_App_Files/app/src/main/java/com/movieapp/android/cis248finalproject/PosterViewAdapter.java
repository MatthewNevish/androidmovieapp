package com.movieapp.android.cis248finalproject;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */

public class PosterViewAdapter extends RecyclerView.Adapter<PosterViewAdapter.ViewHolder> {

    private ArrayList<PosterImages> movie_posters;
    private Context context;
    private LayoutInflater inflater;

    public PosterViewAdapter(Context context, ArrayList<PosterImages> movie_posters) {
        this.context = context;
        this.movie_posters = movie_posters;
    }

    @Override
    public PosterViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mainposterview, viewGroup, false);
        return new PosterViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder ViewHolder, int i) {
        Picasso.with(context).load(movie_posters.get(i).getPoster_image_url()).into(ViewHolder.posterImage);
    }

    @Override
    public int getItemCount() {
        return movie_posters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView posterImage;

        public ViewHolder(View itemView) {
            super(itemView);
            posterImage = (ImageView) itemView.findViewById(R.id.poster_img);
            itemView.setOnClickListener(MainActivity.onClickListener);
        }
    }
}


