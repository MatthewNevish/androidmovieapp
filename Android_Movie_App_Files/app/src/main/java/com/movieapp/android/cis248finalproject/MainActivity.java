package com.movieapp.android.cis248finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */

public class MainActivity extends AppCompatActivity {

    final static String baseURL = "http://image.tmdb.org/t/p/w185/";
    public static View.OnClickListener onClickListener;

    private static RecyclerView recyclerView;

    final static String moviePosters[] = {
            "/nHXiMnWUAUba2LZ0dFkNDVdvJ1o.jpg",
            "/gri0DDxsERr6B2sOR1fGLxLpSLx.jpg",
            "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg",
            "/xGgg2UI20qtEh7mura3RRwP8d3I.jpg",
            "/vR9YvUJCead23MOWwVzv9776eb1.jpg",
            "/h6O5OE3ueRVdCc7V7cwTiQocI7D.jpg",
            "/kqjL17yufvn9OVLyXYpvtyrFfak.jpg",
            "/5N20rQURev5CNDcMjHVUZhpoCNC.jpg",
            "/z4x0Bp48ar3Mda8KiPD1vwSY3D8.jpg",
            "/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg",
            "/WLQN5aiQG8wc9SeKwixW7pAR8K.jpg",
            "/xfWac8MTYDxujaxgPVcRD9yZaul.jpg",
            "/lFSSLTlFozwpaGlO31OoUeirBgQ.jpg",
            "/qjiskwlV1qQzRCjpV0cL9pEMF9a.jpg",
            "/6w1VjTPTjTaA5oNvsAg0y4H6bou.jpg",
            "/b9uYMMbm87IBFOq59pppvkkkgNg.jpg",
            "/jjBgi2r5cRt36xF6iNUEhzscEcb.jpg",
            "/cparcxTFuHdlSOP3MJOpN7Ec9NB.jpg",
            "/gj282Pniaa78ZJfbaixyLXnXEDI.jpg",
            "/z09QAf8WbZncbitewNk6lKYMZsh.jpg",
    };

    final static int movieIDS[] = {
            346672,
            259316,
            297761,
            329865,
            283366,
            363676,
            76341,
            271110,
            277834,
            157336,
            328111,
            284052,
            324668,
            330459,
            262500,
            245891,
            135397,
            333484,
            131631,
            127380
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        posterView();
    }

    private void posterView(){
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        onClickListener = new MyOnClickListener();
        ArrayList posterImages = prepareData();
        PosterViewAdapter adapter = new PosterViewAdapter(getApplicationContext(), posterImages);
        recyclerView.setAdapter(adapter);
    }

    private class MyOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int selectedPosterMovieID = getMovieID(view);
            showPoster(selectedPosterMovieID);
        }
    }

    private int getMovieID (View view) {
        int selectedPoster = recyclerView.getChildLayoutPosition(view);
        int movieID = movieIDS[selectedPoster];
        return movieID;
    }

    private void showPoster(int movieID) {
        Intent intent = new Intent(this, MainActivity2.class);
        intent.putExtra("movieID", movieID);
        startActivity(intent);
    }




private ArrayList prepareData(){

        ArrayList poster_images = new ArrayList<>();
        for(int i=0;i<moviePosters.length;i++){
            PosterImages posterImage = new PosterImages();
            posterImage.setPoster_image_url(baseURL + moviePosters[i]);
            poster_images.add(posterImage);
        }
        return poster_images;
    }
}

