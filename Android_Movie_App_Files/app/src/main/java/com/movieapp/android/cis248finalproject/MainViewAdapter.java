package com.movieapp.android.cis248finalproject;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Matthew Nevish on 12/12/2016.
 * Created for the Final Project for CIS248 at Great Bay Community College
 */

public class MainViewAdapter extends RecyclerView.Adapter<MainViewAdapter.ViewHolder> {
    private ArrayList<PosterImages> movie_posters;
    private Context context;

    public MainViewAdapter(Context context,ArrayList<PosterImages> movie_posters) {
        this.context = context;
        this.movie_posters = movie_posters;

    }

    @Override
    public MainViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mainposterview, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Picasso.with(context).load(movie_posters.get(i).getPoster_image_url()).into(viewHolder.poster_img);
    }

    @Override
    public int getItemCount() {
        return movie_posters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView poster_img;
        public ViewHolder(View view) {
            super(view);
            poster_img = (ImageView)view.findViewById(R.id.poster_img);
        }
    }
}

